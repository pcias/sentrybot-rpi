
var port=process.env.PORT || 3000;

var express = require('express');
var app = express();
//var http = require('http');

const SerialPort = require('serialport');
const Readline = SerialPort.parsers.Readline;

var portPath = '/dev/cu.usbmodem14101';
var portParams  =  { baudRate: 9600 };

const serialPort = new SerialPort(portPath, portParams);
const parser = new Readline();
serialPort.pipe(parser);


app.set('port', port);
//app.set('ipaddr', process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1");
//app.use(express.bodyParser());
//app.use(express.methodOverride());



// app.all('/',function(req,res,next) {
// //    next();
//     res.sendfile(__dirname +'/static/dashboard.html');
// });

app.get('/rpi/:motor/:command', function(req, res){

  let comm = 'c'+req.params.motor+req.params.command;

  console.log('Motor: '+req.params.motor+' Command: '+req.params.command + '->'+comm);
  
  serialPort.write(comm);
  parser.on('data', console.log)
  
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('command: ' + comm +  '  sent\n');
});

app.listen(port, () => console.log(`sentrybot listening on port ${port}`))
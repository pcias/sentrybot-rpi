#include <AFMotor.h>
AF_DCMotor motor1(1, MOTOR12_1KHZ); // create motor #1, 64KHz pwm
AF_DCMotor motor2(2, MOTOR12_1KHZ); // create motor #2, 64KHz pwm
AF_DCMotor motor3(3, MOTOR12_1KHZ); // create motor #3, 64KHz pwm
AF_DCMotor motor4(4, MOTOR12_1KHZ); // create motor #4, 64KHz pwm 


       void prerr(char * message) {
            Serial.print("M:");
            Serial.println(message);
       }
 

       AF_DCMotor* setMotorPTR(int motNum) {
              switch (motNum) {
                    case 1: 
                           return &motor1;
                           break;
                    case 2: 
                           return &motor2;
                           break;
                    case 3: 
                           return &motor3;
                           break;
                    case 4: 
                           return &motor4;
                           break;
                     default:
                           prerr("setMotorPTR: no such motor");

              }
              
       }        

      void motorSetSpeed(int motNum, int speed) {
         AF_DCMotor* motorPTR;
         motorPTR = setMotorPTR(motNum);
         motorPTR->setSpeed(speed);
      }

      

      void motorForward(int motNum) {
         AF_DCMotor* motorPTR;
         motorPTR = setMotorPTR(motNum);
         motorPTR->run(FORWARD);
      }

      void motorBackward(int motNum) {
         AF_DCMotor* motorPTR;
         motorPTR = setMotorPTR(motNum);
         motorPTR->run(BACKWARD);
      }

      void motorRelease(int motNum) {
         AF_DCMotor* motorPTR;
         motorPTR = setMotorPTR(motNum);
         motorPTR->run(RELEASE);
      }


      void parse() {
        char commandPrefix,commandCom;
        int commandMotor, commandParam;
        
        while (Serial.available() > 0) {
             commandPrefix = Serial.read();
             if(commandPrefix = 'c') {
                  commandMotor = Serial.parseInt();
                  if(commandMotor >= 1 && commandMotor <= 4) {
                     
                    //while((commandCom = Serial.read()) = -1);
                     commandCom = Serial.read();
                     
                     switch(commandCom) {
                          case 's':
                          //speed
                          commandParam = Serial.parseInt();
                          if(commandParam >= 0 && commandParam <= 255) {
                                //execute setspeed 
                                motorSetSpeed(commandMotor, commandParam);
                                Serial.print("speed set to ");
                                Serial.println(commandParam);
                          } 
                          else 
                               prerr("speed out of range");
                          break;
                          

                          case 'f':
                           //forward
                           motorForward(commandMotor);
                           break;
                       
                          case 'b':
                          //backward
                          motorBackward(commandMotor);
                          break;
                       
                          case 'r':
                          //release
                          motorRelease(commandMotor);
                          break;
   
                          default:
                          //unknown command
                          prerr("Unknown command.");
                   }
                  }
                   else 
                          prerr("Motor number out of range"); 
             }
             else
               prerr("C prefix command expected");
        }
      }
     
            
void setup () {
 Serial.begin(9600);    
} 

void loop () {
                   parse();
}
